<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\book5;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;  
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;


class Book5Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$id = 1; //or later change to 2
      $id = Auth::id();
      if (Gate::denies('manager')) {
        $boss = DB::table('employees')->where('employee',$id)->first();
        $id = $boss->manager;
    }
      $book5s = User::find($id)->book5s; 
      return view('book5s.index',['book5s' => $book5s]); //return view('todos.index', compact('todos')); //compact(todos) equivalent to ['books' => $books]
  
  //       $user = User::find($id);
  //      $todos = $user->todos;
  //      return view('book5s.index',['book5s' => $book5s]);
  
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create todos..");
        }
        return view ('book5s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  validation
        $this->validate($request,[
            'title'=>'required',
            'author'=>'required']);

        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $book5= new Book5();
        //$id=1;
        $id = Auth::id();
        $book5->title = $request->title;
        $book5->author = $request->author;
        $book5->user_id=$id;
        $book5->status=0;
        $book5->save();
        return redirect('book5s');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $book5 = Book5::find($id);
        return view('book5s.edit',compact('book5'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
   //  public function update(Request $request, $id)
   // {
    //    $book5 = Book5::find($id);
   //           $book5->update($request->all());
   //           return redirect('book5s');
   // }

    public function update(Request $request, $id)
    {
        //only if this book belongs to user
        $book5 = book5::find($id); //$todo = Todo::findOrFail($id);

         //employees are not allowed to change the title 
         if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }   

        //make sure the book belongs to the logged in user
        if(!$book5->user->id == Auth::id()) return(redirect('book5s'));

        //test if title is dirty
        $book5->update($request->except(['_token']));

        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('book5s');
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $book5 = Book5::find($id);
        $book5->delete();
        return redirect('book5s');
    }
}