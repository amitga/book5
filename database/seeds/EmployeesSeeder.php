<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    
                    'name' => 'moshe Muallem',
                    'email' => 'moshe@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role'=> 'employee',
                ],
                [
                    
                    'name' => 'yuda Siani',
                    'email' => 'yuda@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role'=> 'employee',
                ],
                [
                    
                    'name' => 'aner Sabtai',
                    'email' => 'aner@gmail.com',
                    'password' =>  Hash::make('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role'=> 'employee',
                ],
            ]);
    }
}
